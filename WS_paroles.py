#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
NOM : WS_paroles
ROLE : A partir d'un prénom et d'un artiste donnés, récupère toutes les punchlines de cet artiste qui commencent par une des lettres du prénom
"""

import requests
from bs4 import BeautifulSoup
import time
import math
import random
import unicodedata

#Créer la liste qui va regrouper tous les liens pour chaque chanson
links = []
linksDone = []


"""
Affecte une valeur au prénom, à l'artiste et au choix de l'utilisateur
"""

try:  
  """
  QUAND : Le scraping est relancé suite à une requête échouée
  ROLE : Récupère le prénom, l'artiste et le choix de l'utilisateur ainsi que les URL des chansons qui ont déjà été scrappées,
  AFFICHAGE : Message de reprise du programme, Paramètres du programme
  """

  with open('urlDone.txt', 'r') as urlDone :
    contenu = urlDone.readlines()

  if len(contenu) != 0:
    # Récupère le prénom, l'artiste et le choix de l'utilisateur
    lstEntete = contenu[0].strip('\n').split(';')

    prenom = lstEntete[0]
    artiste = lstEntete[1]
    userChoice = lstEntete[2]

    # Récupère les URL des chansons qui ont déjà été scrappées
    for row in contenu[1:] :
        linksDone.append(row.strip())

  # Initialise le compteur de chansons scrappées
  counter = len(linksDone)

  # Affiche le message de reprise du programme ainsi que le prénom, l'artiste et le choix de l'utilisateur
  print("Reprise du programme ... \n")
  print("Paramètres actuels :")
  print("Prénom : " + prenom)
  print("Artiste : " + artiste)
  print("Chansons à scraper : " + userChoice)


except: 
  """
  QUAND : Le scraping est lancé pour la première fois
  ROLE : Récupère le prénom, l'artiste et le choix de l'utilisateur
  AFFICHAGE : Message de bienvenue
  """

  # Affiche le message de bienvenue
  print("Bienvenue sur l'algorthme d'anagrammage !\nCet algorithme récupère les punchlines des chansons de ton artiste préféré.e qui commencent par les lettres de ton prénom.\n")
  
  # Récupère le prenom
  prenom = input("\nDonne le prenom à anagrammer : ")  
  prenom = unicodedata.normalize('NFD', prenom.lower()).encode('ascii', 'ignore').decode('UTF-8') # Supprime les accents du prénom si il y en a

  # Récupère l'artiste
  artiste = input("Donne le nom de ton artiste préféré : ")
  artiste = unicodedata.normalize('NFD', artiste.replace(' ','-').replace("'",'-').replace('.','-').replace('$','').lower()).encode('ascii', 'ignore').decode('UTF-8') # Supprime les accents de l'artiste si il y en a

  # Récupère le choix de l'utilisateur
  userChoice = input("Voulez-vous les punchlines de toutes les chansons ? Les plus populaires ou les nouvelles ?\nEcrire l'un des mots suivant : toutes, populaires, nouvelles\n")
  while userChoice.lower() not in ['toutes', 'toute', 'tout', 'populaires', 'populaire', 'nouvelles', 'nouvelle']:
    userChoice = input("Pas compris ... écrit l'un des 3 mots suivants : toutes, populaires, nouvelles\n")

  # Ecrit les paramètres du programme dans la première ligne du fichier qui stockera les URL déjà scrapés
  with open('urlDone.txt','w') as urlDoneW:
    urlDoneW.write(prenom + ';' + artiste + ';' + userChoice + '\n')

  # Initialise le compteur des chansons scrapées à 0
  counter = 0


"""
Récupère les liens des chansons à scraper
"""

mainURL = 'https://www.paroles.net/' + artiste

mainReponse = requests.get(mainURL)

if mainReponse.ok:

  # Récupère la div principale de la page
  mainSoup = BeautifulSoup(mainReponse.text, 'html.parser')
  mainPager = mainSoup.find('div', {'class':'main-pager'})

  if userChoice.lower() in ['toutes', 'toute', 'tout']:
    """
    QUAND : L'utilisateur veut toutes les chansons de l'artiste
    ROLE : Récupère l'URL de toutes les chansons de l'artiste
    """

    try:
      """
      QUAND : Il existe plusieurs pages pour référencer les chansons de l'artiste
      ROLE : Récupère l'URL de toutes les chansons de l'artiste
      """

      # Récupère le nombre des pages avec la liste des chansons
      linksToPages = mainPager.findAll('a', {'class':'pager-letter'})
      numberOfPages = len(linksToPages)
      time.sleep(random.randint(2,5))

      # Parcours toutes les pages avec la liste des chansons
      for i in range(1, numberOfPages):
        url = mainURL + '-' + str(i)
        print(url)

        reponse = requests.get(url)
        if reponse.ok:  # Vérifie que 'reponse' a bien pu accéder à l'URL
          soup = BeautifulSoup(reponse.text, 'html.parser')

          # Récupère les balises qui contiennent le lien des chansons 
          tds = soup.findAll('td', {'class':'song-name'})

          # Parcours les balises qui contiennent le lien des chansons
          for n in range(len(tds)):    
            #Récupère la balise contenant le lien 
            a = tds[n].find('p').find('a')

            try:
              #Récupère le lien
              linkToSong = a['href']

            except TypeError:
              print('type error')

            if 'https://www.paroles.net' + linkToSong not in links and linkToSong[0] != 'h' :
              # Ajoute le lien à la liste des chansons à scraper si il n'est pas déjà dedans et si il est au bon format
              links.append('https://www.paroles.net' + linkToSong) 


    except:
      """
      QUAND : Il existe 1 seul page pour référencer les chansons de l'artiste
      ROLE : Récupère l'URL de toutes les chansons de l'artiste
      """
      reponse = requests.get(mainURL)
      if reponse.ok:  #Vérifie que 'reponse' a bien pu accéder à l'URL
        
        #Récupère la page HTML sous forme de texte
        soup = BeautifulSoup(reponse.text, 'html.parser')
        tds = soup.findAll('td', {'class':'song-name'})

        for n in range(len(tds)):    
          #Récupère la balise contenant le lien
          a = tds[n].find('p').find('a')
          #Récupère le lien
          try:
            linkToSong = a['href']

          except TypeError:
            print('type error')

          if 'https://www.paroles.net' + linkToSong not in links and linkToSong[0] != 'h' :
            # Ajoute le lien à la liste des chansons à scraper si il n'est pas déjà dedans et si il est au bon format
            links.append('https://www.paroles.net' + linkToSong) 


  else:
    """
    QUAND : L'utilisateur veut récupérer les chansons nouvelles ou populaires
    ROLE : Récupère les liens des chansons populaires ou nouvelles
    """

    reponse = requests.get(mainURL)
    if reponse.ok:  #Vérifie que 'reponse' a bien pu accéder à l'URL
      soup = BeautifulSoup(reponse.text, 'html.parser')
      boxs = soup.findAll('div', {'class': 'song-listing-extra'})

      if userChoice.lower() in ['nouvelles', 'nouvelle']:
        # récupère les balises qui contiennt le lien vers les nouvelles chansons
        tds = boxs[0].findAll('td', {'class':'song-name'})

      elif userChoice.lower() in ['populaires', 'populaire']:
        # récupère les balises qui contiennt le lien vers les chansons populaires
        tds = boxs[1].findAll('td', {'class':'song-name'})

      for n in range(len(tds)):    
        #Récupère la balise contenant le lien
        a = tds[n].find('p').find('a')
        #Récupère le lien
        try:
          linkToSong = a['href']

        except TypeError:
          print('type error')

        if 'https://www.paroles.net' + linkToSong not in links and linkToSong[0] != 'h' :
          # Ajoute le lien à la liste des chansons à scraper si il n'est pas déjà dedans et si il est au bon format
          links.append('https://www.paroles.net' + linkToSong) 

  # Affiche les liens qui vont être scrapés
  print("\nVoici la liste des liens vers les chansons qui vont être inspectées !\n")
  print(links)
  print("\nC'est parti ...\n")


  """
  Structure les données en sortie en itérant sur tous les liens
  Ouvre en écriture les fichiers contenant les punchlines pour chaque lettre
  """
  with open('urlDone.txt', 'a') as urlDoneW :
    # Créer les fichiers pour chaque lettre
    dictFiles = {}
    for letter in prenom:
      if letter not in dictFiles and letter.isalpha():
        file = open(letter + '.txt', 'a')
        dictFiles[letter] = file

    for link in links:
      """
      Parcours tous les liens, récupère les punchlines de chaque chanson et les ecrit dans le fichier correspondant
      """
      if link not in linksDone :
        linksDone.append(link)
        urlDoneW.write(link.strip() + "\n")
        
        url2 = link.strip()
        reponse2 = requests.get(url2)
        if reponse2.ok:
          soup2 = BeautifulSoup(reponse2.text, 'html.parser')

          # Récupère le titre de la chanson
          title = soup2.find('h1',{'class':'song-head'}).contents[1].text

          # Récupère le texte de la chanson
          songText = soup2.find('div',{'class':'song-text'})
          
          # Créer la liste des punchlines
          punchlines = songText.text.split('\r')

          # Nettoie la première punchline
          try:
            punchlines[0].split('\n')[1]
            firstPunchline = punchlines[0].split('\n')[1]
            punchlines.pop(0)
            punchlines.insert(0, firstPunchline)

          except IndexError:
              print('Pas de problème dans la première ligne de la chanson')
          

          # Parcours les punchlines
          uniquePunchlines = []

          for punchline in punchlines:

            # Nettoie la punchline
            punchline = punchline.replace('\n','')

            try:
              # Si elle n'est pas déjà dans le fichier, ajoute la punchline au fichier correspondant
              for keyLetter in dictFiles:
                if punchline[0].lower() == keyLetter and punchline not in uniquePunchlines:
                  dictFiles[keyLetter].write(punchline + "\t" + title + "\n")
                  uniquePunchlines.insert(0, punchline)
                
            except IndexError:
                print('pas de punchline :/')


        # Incrémente de 1 le compteur de chanson
        counter += 1
          
        print(url2 + " ... OK! (" + str(counter) + "/" + str(len(links)) + ")")

        #Pour éviter de se faire repérer en tant que robot, on espace les requêtes
        time.sleep(random.randint(2,5))
    

    for keyLetter in dictFiles:
      # Ferme tous les fichiers
      dictFiles[keyLetter].close()


# Affiche le message de fin de programme
print("\nEt voilà le travail ! Maintenant tu n'as plus qu'à récupéré tes punchlines préférées pour créer ton anagramme ;)\nPeut-être que bientôt, c'est l'algorithme qui le fera pour toi\n")
